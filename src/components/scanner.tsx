import React, { useContext, useEffect } from 'react';
import { SafeAreaView, StyleSheet, Text } from 'react-native';
import { Camera, useCameraDevices } from 'react-native-vision-camera';
import { BarcodeFormat, useScanBarcodes } from 'vision-camera-code-scanner';
import { IServer, ServerContextType } from '../@types/server';
import { ServerContext } from '../context/serverContext';

const Scanner = () => {
  const [hasPermission, setHasPermission] = React.useState(false);
  React.useEffect(() => {
    (async () => {
      const status = await Camera.requestCameraPermission();
      setHasPermission(status === 'authorized');
    })();
  }, []);

  const devices = useCameraDevices();
  const device = devices.back;

  const [frameProcessor, qrCodes] = useScanBarcodes([BarcodeFormat.QR_CODE], {
    checkInverted: false,
  });

  const { server, setServer } = useContext(ServerContext) as ServerContextType;

  useEffect(() => {
    console.log('[' + new Date(Date.now()).toISOString() + '] ', qrCodes);
    if (qrCodes[0] !== undefined && qrCodes[0].displayValue !== undefined) {
      const serverData = JSON.parse(qrCodes[0].displayValue);
      const srv: IServer = {
        url: serverData.url,
        authKey: serverData.auth_key,
        deviceId: 'deviceId',
      };
      setServer(srv);
    }
  }, [qrCodes, setServer]);

  useEffect(() => {
    console.log('[' + new Date(Date.now()).toISOString() + '] ', server);
    fetch(server.url + 'add_song', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Auth-Key': server.authKey,
      },
      body: JSON.stringify({
        client_id: server.deviceId,
        song_id: '3456',
      }),
    })
      .then(() => console.log('Todo bien'))
      .catch(() => console.log('Algo falló'));
  }, [server]);

  return (
    <SafeAreaView style={styles.container}>
      {device != null && hasPermission && (
        <>
          <Camera
            style={StyleSheet.absoluteFill}
            device={device}
            isActive={true}
            frameProcessor={frameProcessor}
            frameProcessorFps={15}
          />
          {qrCodes.map((qrCode, idx) => (
            <Text key={idx} style={styles.barcodeTextURL}>
              {qrCode.displayValue}
            </Text>
          ))}
        </>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  barcodeTextURL: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
});

export default Scanner;
