import React, { createContext, FC, ReactNode, useState } from 'react';
import { ServerContextType, IServer } from '../@types/server';

export const ServerContext = createContext<ServerContextType | null>(null);

const ServerProvider: FC<ReactNode> = ({ children }) => {
  const [server, setServer] = useState<IServer>({} as IServer);

  return (
    <ServerContext.Provider value={{ server, setServer }}>
      {children}
    </ServerContext.Provider>
  );
};

export default ServerProvider;
