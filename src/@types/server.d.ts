export interface IServer {
  url: string;
  authKey: string;
  deviceId: string;
}

export type ServerContextType = {
  server: IServer;
  setServer(server: IServer);
};
