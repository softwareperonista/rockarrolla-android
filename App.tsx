/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import Scanner from './src/components/scanner';
import ServerProvider from './src/context/serverContext';

const App = () => {
  return (
    <ServerProvider>
      <Scanner />
    </ServerProvider>
  );
};

export default App;
